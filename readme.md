to do:
- [ ] bikin demo
- [x] update biar tween nya jalan
- [ ] deploy to heroku
- [ ] update biar ada gambar customnya

# Interactive Parallax Art

Gyro based or mouse movement art, on a stacked layered png. Layers moved based on accelerometer reading.

## Screenshot

![](img/../images/demo.gif)

## Getting Started

see demo [here]()

### Prerequisites

localhost, using live server or any other local hosting services

### Installing

1. Draw something in layers, mainly 8 layers, usually consist of
   1. background layer
   2. mid layer
   3. subject layer
2. Replace the picture layer in image folder
3. Replace the `layer_list` dictionaries as needed


## Built With

JavaScript

## Acknowledgments

* **Jarom Vogel** - *Canvas, gyro work, art* - [web](https://jaromvogel.com/)
